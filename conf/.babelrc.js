const eslintrc = {
  "presets": [
    "es2015",
    "react"
  ],
  "plugins": [
    "transform-object-rest-spread",
    "transform-runtime"
  ],
  "env": {
    "production": {}
  },
}

module.exports = eslintrc
