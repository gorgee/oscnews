import { fetchInterval, fetchTimely } from './fetch';
import BlockFetch from './BlockFetch';

export {
  fetchInterval,
  fetchTimely,
  BlockFetch,
};
